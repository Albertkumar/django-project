from django.db import models


# Create your models here.
class Contact(models.Model):
    firstname = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    employeeid = models.CharField(max_length=50)
    city = models.CharField(max_length=50) 