from django.shortcuts import render, HttpResponse
from home.models import Contact
 
# Create your views here.
def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html')

def services(request):
    return render(request, 'services.html')
   
def contact(request):
    if request.method == "POST":
        firstname = request.POST.get('firstname')
        lastname = request.POST.get('lastname')
        employeeid = request.POST.get('employeeid')
        city = request.POST.get('city')
        contact = Contact(firstname=firstname, lastname=lastname, employeeid=employeeid, city=city)
        contact.save() 
         
    return render(request, 'contact.html')
   
          